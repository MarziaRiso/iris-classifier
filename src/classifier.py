from dataset import *
from utils import *
from PIL import Image
from coder import *

if __name__ == "__main__":
	dataset = create_dataset(DATASET, IMAGE_EXT, MASK_EXT, single=True)

	with open(OUTPUT_BLOB_PATH, 'w', encoding="utf8") as fw:
		for probe in dataset:
			probe_image = Image.open(probe[IMAGE_PATH])
			probe_mask = Image.open((probe[MASK_PATH]))

			probe_code = CoderBlob(probe_image, probe_mask)
			probe_code.code()

			gallery_output = []
			for gallery in dataset:

				gallery_image = Image.open(gallery[IMAGE_PATH])
				gallery_mask = Image.open(gallery[MASK_PATH])

				gallery_code = CoderBlob(gallery_image, gallery_mask)
				gallery_code.code()

				sim_lbp = probe_code.match(gallery_code)
				gallery_output.append(str(sim_lbp))

			print(probe)
			fw.write("\t".join(gallery_output)+"\n")

	write_dataset(dataset, OUTPUT_DATASET)