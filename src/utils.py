DATASET = "../dataset/gallery"

IMAGE_EXT = ".iris.norm.png"
MASK_EXT = ".defects.norm.png"

OUTPUT_BLOB_PATH = "../output/blob_similarity.tsv"
OUTPUT_LBP_PATH = "../output/lbp_similarity.tsv"
OUTPUT_DATASET = "../output/dataset.txt"

SUBJECT = "subject"
EYE = "eye"
IMAGE_PATH = "image_path"
MASK_PATH = "mask_path"


