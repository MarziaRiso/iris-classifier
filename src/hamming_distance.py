import numpy as np
import cv2


def shift_image(image, shift):
	shifted_img = np.ndarray(image.shape)

	for i in range(shifted_img.shape[0]):
		for j in range(shifted_img.shape[1]):
			shifted_img[i, (j + shift) % shifted_img.shape[1]] = image[i, j]

	return shifted_img


def hamming_distance(blob1, mask1, blob2, mask2):
	xor_img = cv2.bitwise_xor(blob1, blob2)
	not_mask1 = cv2.bitwise_not(mask1)
	not_mask2 = cv2.bitwise_not(mask1)

	first_and = cv2.bitwise_and(xor_img, not_mask1)
	second_and = cv2.bitwise_and(first_and, not_mask2) #Numeratore
	or_mask = cv2.bitwise_or(mask1, mask2) #Denominatore

	hd = cv2.countNonZero(second_and) / (blob1.size - - cv2.countNonZero(or_mask))

	return hd


def shifted_hamming_distance(blob1, mask1, blob2, mask2, shift):
	distance = 1.0

	for i in range(-shift, shift):
		shifted_img2 = shift_image(blob2, i)
		shifted_mask2 = shift_image(mask2, i)

		distance = min(distance, hamming_distance(blob1, mask1, shifted_img2, shifted_mask2))

	return distance
