import pandas
import numpy as np
from dataset import *
import matplotlib.pyplot as plt

def compute_far_frr_gar_grr(subjects, content):
	tot_gen = content.shape[0] #Total Genuine
	tot_imp = content.size - tot_gen #Total Impostors

	gars, fars, grrs, frrs = [], [], [], []

	thresholds = np.linspace(0.0, 1.0, 11)
	for tr in thresholds:
		false_acc, false_rej, gen_acc, gen_rej = 0, 0, 0, 0
		for i in range(content.shape[0]):
			for j in range(content.shape[1]):
				if i == j: continue
				if content.iloc[i, j] >= tr: #Considero la similitudine
					if subjects[i] == subjects[j]: gen_acc += 1
					else: false_acc += 1
				else:
					if subjects[i] == subjects[j]:  false_rej += 1
					else: gen_rej += 1

		gars.append(gen_acc / tot_gen)
		fars.append(false_acc / tot_imp)
		frrs.append(false_rej / tot_gen)
		grrs.append(gen_rej / tot_imp)

	return gars, fars, frrs, grrs


def compute_cmc(dataset, scores):
	cms = np.zeros((len(dataset)))

	for i in range(scores.shape[0]):
		probe = dataset[i]
		sorted_list = scores.iloc[i].sort_values(ascending=False)[1:]

		for pos, gallery_idx in enumerate(sorted_list.index):
			gallery = dataset[gallery_idx]
			score = sorted_list.iloc[pos]

			if probe[SUBJECT] == gallery[SUBJECT]:
				cms[pos] += 1
				break

	cms[0] = cms[0] / len(dataset)
	for i in range(1, len(dataset)):
		cms[i] = cms[i] / len(dataset) + cms[i-1]
	return cms


def draw_far_frr(far, frr):
	thresholds = np.linspace(0.0, 1.0, len(far))
	plt.title("FAR/FRR Curves")
	plt.xlim(-0.1, 1.1)
	plt.xlabel("Thresholds")
	plt.ylim(-0.1, 1.1)
	plt.ylabel("Error rate")
	plt.plot(far,thresholds, frr, thresholds)
	plt.show()


def draw_cmc(cmc):
	x = np.arange(0, len(cmc), 1)
	plt.title("Cumulative Match Curve")
	plt.xlim(-1, len(cmc))
	plt.xlabel("# subjects")
	plt.ylim(-0.1, 1.1)
	plt.ylabel("CMS")

	plt.plot(x, cmc)
	plt.show()


if __name__=="__main__":
	dataset = load_dataset(OUTPUT_DATASET)
	print(dataset)

	scores = pandas.read_csv(OUTPUT_BLOB_PATH, sep="\t", header=None)

	gars, fars, frrs, grrs = compute_far_frr_gar_grr(dataset, scores)
	draw_far_frr(fars,frrs)

	cmc = compute_cmc(dataset, scores)
	draw_cmc(cmc)
