from PIL import Image
from log import *
from hamming_distance import *
import cv2

class CoderBlob:
	def __init__(self, iris_image, mask_image, sigmas=[1, 2, 4, 8], shift=5):
		self.iris = np.array(iris_image.convert("L"))
		self.mask = np.array(mask_image) / 255.0
		self.sigmas = sigmas
		self.shift = shift
		self.kernels = init_log_kernels(self.sigmas)

		self.logs = []
		self.bin_merge = []

	def code(self):
		for i, k in enumerate(self.kernels):
			log_k = cv2.filter2D(self.iris, -1, k)
			log_k = cv2.convertScaleAbs(log_k, log_k, self.sigmas[i], 0)
			self.logs.append(log_k)

		merge = merge_logs(self.logs)
		self.bin_merge = binarize_log(merge)

	def match(self, code):
		return 1.0 - shifted_hamming_distance(self.bin_merge, self.mask, code.bin_merge, code.mask, self.shift)


if __name__ == "__main__":
	image1 = Image.open("..\\dataset\\002\\IMG_002_L_1.iris.norm.png")
	mask1 = Image.open("..\\dataset\\002\\IMG_002_L_1.defects.norm.png")
	coder1 = CoderBlob(image1, mask1)
	coder1.code()

	image2 = Image.open("..\\dataset\\002\\IMG_002_L_2.iris.norm.png")
	mask2 = Image.open("..\\dataset\\002\\IMG_002_L_2.defects.norm.png")
	coder2 = CoderBlob(image2, mask2)
	coder2.code()

	distance = coder1.match(coder2)
	print("Final Similarity: ", 1.0 - distance)





