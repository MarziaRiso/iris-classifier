import matplotlib.pyplot as plt
import cv2

def compute_histogram(lbp, show=False):
    n_bins = int(lbp.max() + 1)
    histogram = cv2.calcHist([lbp], [0], None, [n_bins], [0, n_bins])
    if show:
        plt.plot(histogram)
        plt.show()
    return histogram