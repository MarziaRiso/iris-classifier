from coder_lbp import *
from coder_blob import *

class CoderIris:
	def __init__(self, iris_image, mask_image, neighbors=8, radius=1, zones=5, sigmas=[1, 2, 4, 8], shift=5):
		self.iris_image = iris_image
		self.mask_image = mask_image

		self.coder_lbp = CoderLBP(self.iris_image, self.mask_image, neighbors, radius, zones)
		self.coder_blob = CoderBlob(self.iris_image, self.mask_image, sigmas, shift)

	def code(self):
		self.coder_lbp.code()
		self.coder_blob.code()

	def match(self, code):
		similarity_lbp = self.coder_lbp.match(code.coder_lbp)
		similarity_blob = 1.0 - self.coder_blob.match(code.coder_blob)

		return similarity_lbp, similarity_blob