import numpy as np


def get_kernel_size(sigma):
	side = int(4.783 * sigma + 7.044)
	if side % 2 == 0: side += 1
	return side


def compute_log(x, y, sigma):
	return (1.0 / (2.0 * 3.14 * sigma)) * \
	        ((x**2.0 + y**2.0 - 4 * sigma) /
	        (4 * sigma**2)) * \
	        np.exp(-((x**2 + y**2) / (4 * sigma)))


def create_kernel_log(size, sigma):
	kernel = np.ndarray((size, size))
	for i in range(size):
		for j in range(size):
			kernel[i, j] = compute_log(i-size/2, j-size/2, sigma)
	return kernel


def init_log_kernels(sigmas):
	kernels = []
	for i, sigma in enumerate(sigmas):
		kernel_size = get_kernel_size(sigma)
		kernels.append(create_kernel_log(kernel_size, sigma))
	return kernels


def binarize_log(in_log):
	bin_log = np.ndarray(in_log.shape)
	for i in range(bin_log.shape[0]):
		for j in range(bin_log.shape[1]):
			if in_log[i, j] <= 0: bin_log[i, j] = 0
			else: bin_log[i, j] = 255
	return bin_log


def merge_logs(log_imgs):
	merge_log = np.ndarray(log_imgs[0].shape)

	for i in range(merge_log.shape[0]):
		for j in range(merge_log.shape[1]):
			log_max = 0.0
			for k in range(len(log_imgs)):
				log_max = max(log_max, log_imgs[k][i, j])
			merge_log[i, j] = log_max
	return merge_log
