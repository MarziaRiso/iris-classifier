from PIL import Image
import numpy as np
from skimage.feature import local_binary_pattern
import cv2


class CoderLBP:
	def __init__(self, iris_image, mask_image, neighbors=8, radius=1, zones=5):
		self.iris = np.array(iris_image.convert("L"))
		self.mask = (np.array(mask_image) / 255.0).astype('uint8')
		self.neighbors = neighbors
		self.radius = radius
		self.zones = zones
		self.histograms = []

	def code(self):
		zone_rows = self.iris.shape[0] / self.zones;

		for z in range(self.zones):
			start = int(z * zone_rows)
			end = int((z + 1) * zone_rows)
			zone_image = self.iris[start:end, :]
			zone_mask = self.mask[start:end, :]

			lbp = local_binary_pattern(zone_image, self.neighbors, self.radius).astype('float32')

			n_bins = int(lbp.max() + 1)
			histogram = cv2.calcHist([lbp], [0], zone_mask, [n_bins], [0, n_bins])
			histogram = cv2.normalize(histogram, histogram)
			self.histograms.append(histogram)

	def match(self, code):
		zone_rows = self.iris.shape[0] / self.zones
		similarity = 0.0
		for z in range(self.zones):
			similarity += cv2.compareHist(self.histograms[z], code.histograms[z], cv2.HISTCMP_CORREL)

		return similarity / self.zones


if __name__ == "__main__":
	image1 = Image.open("..\\dataset\\001\\IMG_001_L_1.iris.norm.png")
	mask1 = Image.open("..\\dataset\\001\\IMG_001_L_1.defects.norm.png")
	coder1 = CoderLBP(image1, mask1)
	coder1.code()

	image2 = Image.open("..\\dataset\\001\\IMG_001_L_1.iris.norm.png")
	mask2 = Image.open("..\\dataset\\001\\IMG_001_L_1.defects.norm.png")
	coder2 = CoderLBP(image2, mask2)
	coder2.code()

	similarity = coder1.match(coder2)
	print("Final Similarity: ", similarity)





