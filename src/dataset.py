import os
from utils import *


def create_dataset(dataset_path, img_extension, mask_extension, single=False):
	dataset = []
	for subject in os.listdir(dataset_path):
		for eye in os.listdir(os.path.join(dataset_path, subject)):
			samples = os.listdir(os.path.join(dataset_path, subject, eye))
			for image_path in samples:
				if image_path.endswith(img_extension):
					mask_path = image_path[:image_path.find(".")]+mask_extension
					if mask_path in samples:
						image_path = os.path.join(dataset_path, subject, eye, image_path)
						mask_path = os.path.join(dataset_path, subject, eye, mask_path)

						dataset.append({SUBJECT: subject, EYE: eye, IMAGE_PATH: image_path, MASK_PATH : mask_path})

	return dataset


def write_dataset(dataset, dataset_path):
	with open(dataset_path, 'w', encoding="utf8") as fw:
		for i, subject in enumerate(dataset):
			fw.write(f"{i}\t{repr(subject)}\n")


def load_dataset(dataset_path):
	dataset = []
	with open(dataset_path, 'r', encoding="utf8") as fw:
		for line in fw:
			id, subject= line.strip().split("\t")
			dataset.append(eval(subject))
	return dataset

